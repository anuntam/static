# Termeni și condiții

Bine ai venit pe site-ul anuntam.ro, locul unde publici anunțuri de tot felul. Vrem ca experiența ta pe anuntam.ro sa fie una ce îți satisface nevoile de consumator și de utilizator anuntam.ro. Astfel pentru o bună utilizare a acestui serviciu vă rugăm să citiți termenii și condițiile în continuare.

## 1. Termeni folosiți

- **Site** - website-ul anuntam.ro
- **Utilizator** - Orice tip de persoană (înafară de programe ce simulează un utilizator) ce acceasează acest website.
- **Anunț** - Un articol tip anunț postat pe website cu scopul de a anunța un produs vândut, un serviciu sau închiriere de bunuri materiale.


## 2. Continut anunțuri

Tinem foarte mult la calitatea continutului, astfel vrem să verificăm prin diferite metode că aceasta este astfel cum utilizatorii acestui site vor a fii. Anunturile ce conțin informații contrare calității pe care o dorim vor fi raportate de către utilizatori și de căte site prin metode automate.