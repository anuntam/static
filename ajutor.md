# Ajutor

## Postarea unui anunț

Pentru a posta un anunț da-ți click pe [postează anunț](https://anuntam.ro/adauga), alege categoria în care vrei să postezi și completeaza câmpurile din formular. Acest site este în versiunea beta, iar unele din funcționalitățiile pe care ți le-ai dorii ar putea să lipsească, astfel vă rugăm să ne contactați la cabytes@gmx.com pentru a îmbunătăți acest website.
