# Documentatia pentru dezvoltatorii web

Aceasta pagina contine documentatia necesara pentru manipularea datelor de pe website-ul anuntam.ro, deocamdata acest website este in constructie si ne dorim ca tu sa fi cel ce solicita functiile acestui website. Daca ai nevoie de anumite functionalitati API, va rugam sa ne contactati la anuntam@gmx.com. Va multumim

## Creare anunt

**Endpoint:**

```
POST https://anuntam.ro/api/1/ads
```

**Payload:**

```
{
    "category": 1,
    "title": "Titlu anunt",
    "description": "Descriere anunt",
    "tags": "a,b,c",
    "city": 1000,
    "price": 1000,
    "props": {
        "rooms": 2,
    }
}
```

Pentru a stabili valoarea atributului city va rugam folositi ```/api/1/counties``` si ```/api/1/counties/{county}/cities```, daca acesta nu este stabilit orasul utilizatorului v-a fi folosit.

Pentru a stabili valoarea categoriei, va rugam folositi acest endpoint `/api/1/ads/categories` unde este afisata lista de categorii.

Pentru stabilirea proprietatiilor aferente unui anunt va rugam verificati care sunt disponibile pentru categoria in care vrei sa adaugi `/api/1/ads/categories/{category}/props`

## Listare anunturi

**Endpoint:**

```
GET https://anuntam.ro/api/1/ads
```

**Raspuns:***

```
[
    {
        "id": 1,
        "title": "Titlu anunt",
        ...
    }
]
```
